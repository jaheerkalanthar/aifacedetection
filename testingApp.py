import cv2
import random
#It contain trained Data
trainedFaceData=cv2.CascadeClassifier("haarcascade_frontalface_default.xml")#CascadeClassifier is used to detect the face (i.e) it classifies something as face thats all.
#image=cv2.imread('images/Screenshot_20210331-223259.jpg')#Choose an image to detect face

#Capture video from webCam
webCam=cv2.VideoCapture(0)#0 is used to use default camera

while True:#We use While loop because we should iterate the loop until terminate the video

    successfullFrameRead,frame=webCam.read()

    #successfullFrameRead ,It is return the data in boolean Formate
    #frame is returned actual code frame co-ordinates
    greyScaledImage=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)#We can change image into grey in here too

    faceCo_ordinates=trainedFaceData.detectMultiScale(greyScaledImage)#Detect Face-->It will returns the face coOrdinates 
    #The above code returns [[x,y,w,h]] co-ordinates as list
    

    #we shoud Plug in face detection
    for i in range(len(faceCo_ordinates)):#This one is detect all faces in the images
    #for (x,y,w,h) in faceCo_ordinates: we can also write like that 
    
        x,y,w,h=faceCo_ordinates[i]

        cv2.rectangle(frame,(x,y),(x+w,y+h),(random.randrange(256),random.randrange(256),random.randrange(256)), 2)

    imageShow=cv2.imshow("MyImage",frame)
    #Itg will pause execution of code until we press any key otherwise it will gone
    
    key=cv2.waitKey(1)#One because each frame is waiting for just one millisecond
#First Try we should give images in black and white it is easy to identify

#We should train our image to the cv algorithm

#print(faceCo_ordinates)
#Draw rectangle in the face

    if key==81 or key==113:
        break

print("Compiled")